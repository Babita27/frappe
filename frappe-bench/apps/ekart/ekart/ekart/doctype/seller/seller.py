# -*- coding: utf-8 -*-
# Copyright (c) 2018, Frappe and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.utils import validate_email_add
from frappe.website.website_generator import WebsiteGenerator

class Seller(WebsiteGenerator):
    def validate(self):
        validate_email_add(self.email_id,True)
        self.validate_password()

    def validate_password(self):
        if(len(self.password)<8):
            frappe.throw("Password must be atleast of 8 characters")
        elif(self.password!=self.cpassword):
            frappe.throw("Password does not match")
